#!/usr/bin/env python3
"""
"""
import asyncio
import os
import sys
import subprocess
from asyncqt import QEventLoop, asyncSlot, asyncClose
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import (
    QApplication, QCheckBox, QComboBox, QWidget,
    QDialogButtonBox, QDesktopWidget
)
from PySide2.QtGui import QIcon
from PySide2.QtCore import QMetaObject
from serial.tools.list_ports import grep
from serial.tools.list_ports_posix import comports

# Load in a ui file from ../resource/
DIR = os.path.dirname(os.path.realpath(__file__))
ui_file = DIR + "/../resource/launcher.ui"
icon_file = DIR + "/../resource/Interbotix_Logo.png"

# from __future__ import print_function
# if sys.version_info[:3] == (2, 7, 3):

arms = {
    'ReactorX 150': 'rx150',
    'WidowX 200': 'wx200',
    'WidowX 250': 'wx250',
}

args = {
    'arg_gazebo': 'simulate',
    'arg_moveit': 'use_moveit',
    'arg_platform': 'use_platform',
    'arg_teleop': 'teleop',
    'arg_control_gui': 'control_gui',
    'arg_joint_state_gui': 'gui_joint_state',
}


def formatCmd(settings):
    # roslaunch interbotix_{robot_name}_bringup full.launch arg:=value ...
    base = ("roslaunch interbotix_{robot_name}_bringup full.launch {args}"
            " --screen")
    arg = " {arg}:={value}"

    robot_name = arms[settings['robot_name']]
    s_args = ''
    for key in settings.keys():
        if 'arg_' in key:
            s_args += arg.format(
                arg=args[key],
                value=settings[key],
            )
        if key == 'port' and settings[key] != "":
            s_args += arg.format(
                arg="dynamixel_usb_port",
                value=settings[key],
            )

    string = base.format(robot_name=robot_name,
                         args=s_args)

    return string


def list_ports(regex=None, include_links=None):
    # https://github.com/pyserial/pyserial/blob/master/serial/tools/list_ports.py#L53
    hits = []
    if regex:
        iterator = sorted(grep(regex, include_links=include_links))
    else:
        iterator = sorted(comports(include_links=include_links))

    for n, (port, desc, hwid) in enumerate(iterator, 1):
        hits.append((port, desc, hwid))

    return hits


class UiLoader(QUiLoader):
    # https://github.com/bpabel/pysideuic/blob/master/pysideuic/__init__.py#L46
    def __init__(self, baseinstance):
        QUiLoader.__init__(self, baseinstance)
        self.baseinstance = baseinstance

    def createWidget(self, class_name, parent=None, name=''):
        if parent is None and self.baseinstance:
            return self.baseinstance
        else:
            widget = QUiLoader.createWidget(self, class_name, parent, name)
            if self.baseinstance:
                setattr(self.baseinstance, name, widget)
            return widget


def loadUi(uifile, baseinstance=None):
    # https://github.com/bpabel/pysideuic/blob/master/pysideuic/__init__.py#L86
    loader = UiLoader(baseinstance)
    widget = loader.load(uifile)
    QMetaObject.connectSlotsByName(widget)
    return widget


class Launcher(QWidget):
    def __init__(self, parent=None, loop=None):
        super(Launcher, self).__init__(parent)

        loadUi(ui_file, self)
        self.setWindowIcon(QIcon(icon_file))

        self.settings = dict.fromkeys(args.keys(), False)
        self.settings.update({
                'robot_name': 'WidowX 200',
                'arg_moveit': True,
                'arg_platform': True,
                'arg_control_gui': True,
        })

        for arg in args.keys():
            obj = self.findChild(QCheckBox, arg)
            if obj is None:
                print('Failed to find ui for ' + arg)
                continue
            obj.setChecked(self.settings.get(arg, False))
            obj.clicked.connect(self.make_arg_handler(arg, obj))

        robot_name = self.findChild(QComboBox, 'box_robot')
        robot_name.addItems(list(arms.keys()))
        robot_name.setCurrentText(self.settings['robot_name'])
        robot_name.currentIndexChanged.connect(
                self.make_robot_name_handler(robot_name))

        ok_cancel = self.findChild(QDialogButtonBox, 'buttonBox')
        ok_cancel.button(ok_cancel.Ok).clicked.connect(self.submit)
        ok_cancel.button(ok_cancel.Cancel).clicked.connect(self.close)

        self.ports = []
        self.port_box = self.findChild(QComboBox, 'box_port')
        self.port_box.addItems(self.ports)
        self.port_box.currentIndexChanged.connect(
                self.make_port_box_handler(self.port_box))

        loop.create_task(self.list_ports())

    @asyncClose
    async def closeEvent(self, event):
        pass

    def submit(self):
        command = formatCmd(self.settings)
        print('\n\n' + command + '\n\n')
        self.close()
        p = subprocess.Popen(command, shell=True)
        try:
            p.wait()
        except KeyboardInterrupt:
            try:
                p.terminate()
            except OSError:
                pass
            p.wait()

    def make_arg_handler(self, arg, obj):
        @asyncSlot()
        async def arg_handler():
            self.settings[arg] = obj.isChecked()
        return arg_handler

    def make_robot_name_handler(self, obj):
        @asyncSlot()
        async def arg_handler(index):
            package = obj.currentText()
            self.settings['robot_name'] = package
        return arg_handler

    def make_port_box_handler(self, obj):
        @asyncSlot()
        async def arg_handler(index):
            self.settings['port'] = obj.currentText()
        return arg_handler

    @asyncio.coroutine
    def list_ports(self):
        while True:
            self.ports = list_ports()
            self.port_box.clear()
            self.port_box.addItems([p[0] for p in self.ports])
            yield from asyncio.sleep(1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)

    launcher = Launcher(loop=loop)

    # Center the dialog on the current screen
    rect = launcher.frameGeometry()
    center = QDesktopWidget(launcher).availableGeometry(launcher).center()
    rect.moveCenter(center)
    launcher.move(rect.topLeft())

    launcher.show()

    try:
        # choice = loop.run_until_complete(main())
        # choice = loop.run_until_complete(launcher.show(lj))
        sys.exit(loop.run_forever())
    finally:
        loop.close()

    sys.exit(0)
