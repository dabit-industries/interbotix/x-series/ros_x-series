/*******************************************************************************
* Copyright 2018 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Authors: Darby Lim, Hye-Jong KIM, Ryan Shim, Yong-Ho Na */

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include "../include/interbotix_x-series_control_gui/main_window.hpp"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace interbotix_xseries_control_gui {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/

MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
  : QMainWindow(parent)
  , qnode(argc,argv)
{
  ui.setupUi(this); // Calling this incidentally connects all ui's triggers to on_...() callbacks in this class.
  QObject::connect(ui.actionAbout_Qt, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt())); // qApp is a global variable for the application
  //connect(ui.tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabSelected()));
  QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

  qnode.init();

  timer_start();
  on_btn_actuator_enable_clicked();

}

MainWindow::~MainWindow() {}

void MainWindow::timerCallback()
{
  std::vector<double> joint_angle = qnode.getPresentJointAngle();
  if(joint_angle.size() != 6)
    return;

  ui.txt_j1->setText(QString::number(joint_angle.at(0),'f', 3));
  ui.txt_j2->setText(QString::number(joint_angle.at(1),'f', 3));
  ui.txt_j3->setText(QString::number(joint_angle.at(2),'f', 3));
  ui.txt_j4->setText(QString::number(joint_angle.at(3),'f', 3));
  ui.txt_j5->setText(QString::number(joint_angle.at(4),'f', 3));
  ui.txt_grip->setText(QString::number(joint_angle.at(5),'f', 3));

  if(qnode.getOpenManipulatorActuatorState() == true){
    ui.btn_actuator_enable->setEnabled(false);
    ui.btn_actuator_disable->setEnabled(true);
  }else{
    ui.btn_actuator_enable->setEnabled(true);
    ui.btn_actuator_disable->setEnabled(false);
  }

  std::vector<double> position = qnode.getPresentKinematicsPose();
  if(position.size() != 3)
    return;

  ui.txt_x->setText(QString::number(position.at(0),'f', 3));
  ui.txt_y->setText(QString::number(position.at(1),'f', 3));
  ui.txt_z->setText(QString::number(position.at(2),'f', 3));

}

void MainWindow::writeLog(QString str)
{
  ui.plainTextEdit_log->moveCursor (QTextCursor::End);
  ui.plainTextEdit_log->appendPlainText(str);
}

void MainWindow::timer_start(void)
{
  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(timerCallback()));
  timer->start(100);

  writeLog("QTimer start : 100ms");
  //ui.btn_timer_start->setEnabled(false);
  ui.btn_actuator_disable->setEnabled(true);
  ui.btn_actuator_enable->setEnabled(true);
  ui.btn_end_effector_close->setEnabled(true);
  ui.btn_end_effector_open->setEnabled(true);
  ui.btn_home_pose->setEnabled(true);
  ui.btn_init_pose->setEnabled(true);
  ui.btn_up_pose->setEnabled(true);
  ui.btn_read_joint_angle->setEnabled(true);
  //ui.btn_read_kinematic_pose->setEnabled(true);
  ui.btn_send_joint_angle->setEnabled(true);
  //ui.btn_send_kinematic_pose->setEnabled(true);
  //ui.btn_send_drawing_trajectory->setEnabled(true);
  //ui.btn_set_end_effector->setenabled(true);
}

void MainWindow::on_btn_actuator_enable_clicked(void)
{
  if(!qnode.setActuatorState(true))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }

  writeLog("Send actuator state to enable");
}

void MainWindow::on_btn_actuator_disable_clicked(void)
{
  if(!qnode.setActuatorState(false))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }

  writeLog("Send actuator state to disable");
}

void MainWindow::on_btn_init_pose_clicked(void)
{
  std::vector<std::string> joint_name;
  std::vector<double> joint_angle;
  double path_time = 2.0;
  joint_name.push_back("joint1"); joint_angle.push_back(0.0);
  joint_name.push_back("joint2"); joint_angle.push_back(0.0);
  joint_name.push_back("joint3"); joint_angle.push_back(0.0);
  joint_name.push_back("joint4"); joint_angle.push_back(0.0);
  joint_name.push_back("joint5"); joint_angle.push_back(0.0);

  if(!qnode.setJointSpacePath(joint_name, joint_angle, path_time))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }

  writeLog("Send joint angle to initial pose");
}

void MainWindow::on_btn_home_pose_clicked(void)
{
  std::vector<std::string> joint_name;
  std::vector<double> joint_angle;
  double path_time = 2.0;

  joint_name.push_back("joint1"); joint_angle.push_back(0.0);
  joint_name.push_back("joint2"); joint_angle.push_back(-1.85);
  joint_name.push_back("joint3"); joint_angle.push_back(1.6);
  joint_name.push_back("joint4"); joint_angle.push_back(0.5);
  joint_name.push_back("joint5"); joint_angle.push_back(0.0);
  if(!qnode.setJointSpacePath(joint_name, joint_angle, path_time))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }
  writeLog("Send joint angle to home pose");
}

void MainWindow::on_btn_up_pose_clicked(void)
{
  std::vector<std::string> joint_name;
  std::vector<double> joint_angle;
  double path_time = 2.0;

  joint_name.push_back("joint1"); joint_angle.push_back(0.0);
  joint_name.push_back("joint2"); joint_angle.push_back(0.0);
  joint_name.push_back("joint3"); joint_angle.push_back(-1.5);
  joint_name.push_back("joint4"); joint_angle.push_back(0.0);
  joint_name.push_back("joint5"); joint_angle.push_back(0.0);
  if(!qnode.setJointSpacePath(joint_name, joint_angle, path_time))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }
  writeLog("Send joint angle to home pose");
}

void MainWindow::on_btn_end_effector_open_clicked(void)
{
  std::vector<double> joint_angle;
  joint_angle.push_back(0.017);

  if(!qnode.setToolControl(joint_angle))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }

  writeLog("Send end_effector open");
}

void MainWindow::on_btn_end_effector_close_clicked(void)
{
  std::vector<double> joint_angle;
  joint_angle.push_back(-0.013);
  if(!qnode.setToolControl(joint_angle))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }

  writeLog("Send end_effector close");
}


void MainWindow::on_btn_read_joint_angle_clicked(void)
{
  std::vector<double> joint_angle = qnode.getPresentJointAngle();
  ui.doubleSpinBox_j1->setValue(joint_angle.at(0));
  ui.doubleSpinBox_j2->setValue(joint_angle.at(1));
  ui.doubleSpinBox_j3->setValue(joint_angle.at(2));
  ui.doubleSpinBox_j4->setValue(joint_angle.at(3));
  ui.doubleSpinBox_j5->setValue(joint_angle.at(4));
  ui.doubleSpinBox_end_effector->setValue(joint_angle.at(5));

  writeLog("Read joint angle");
}
void MainWindow::on_btn_send_joint_angle_clicked(void)
{
  std::vector<std::string> joint_name;
  std::vector<double> joint_angle;
  double path_time = ui.doubleSpinBox_time_js->value();

  joint_name.push_back("joint1"); joint_angle.push_back(ui.doubleSpinBox_j1->value());
  joint_name.push_back("joint2"); joint_angle.push_back(ui.doubleSpinBox_j2->value());
  joint_name.push_back("joint3"); joint_angle.push_back(ui.doubleSpinBox_j3->value());
  joint_name.push_back("joint4"); joint_angle.push_back(ui.doubleSpinBox_j4->value());
  joint_name.push_back("joint5"); joint_angle.push_back(ui.doubleSpinBox_j5->value());

  set_end_effector();

  if(!qnode.setJointSpacePath(joint_name, joint_angle, path_time))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }

  writeLog("Send joint angle");
}
void MainWindow::set_end_effector(void)
{
  std::vector<double> joint_angle;
  joint_angle.push_back(ui.doubleSpinBox_end_effector->value());
  if(!qnode.setToolControl(joint_angle))
  {
    writeLog("[ERR!!] Failed to send service");
    return;
  }
  writeLog("Send end_effector value");
}

}  // namespace interbotix_xseries_control_gui

