#!/usr/bin/env python2.7
# -*- encoding: utf-8 -*-
'''
Copyright (c) 2019, Dabit Industries, LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
import rospy
import select
import sys
import termios
import tty
from sensor_msgs.msg import JointState
from std_msgs.msg import Header

debug = False
speed = 0.04

usage = """
Control your robot arm!
-------------------------
Joint: Key Down / Key Up
---
        Base:  z / x
    Shoulder:  a / s
       Elbow:  q / w
       Wrist:  c / v
End Effector:  d / f
     Gripper:  e / r

Note: Due to terminal limitations, only one key can be pressed at a time.
      Use the GUI instead:  `rosrun teleop_magic keyboard_teleop --gui`
CTRL + C to quit
"""

# TODO: ordered dict
# TODO: handle limits?
joint_bindings = {
    'joint1': {
        'keys': ['z', 'x'],
        'position': 0,
        'limit': [-2.83, 2.83],
        'multiplier': speed,
    },
    'joint2': {
        'keys': ['a', 's'],
        'position': 0,
        'limit': [-1.79, 1.57],
        'multiplier': speed,
    },
    'joint3': {
        'keys': ['q', 'w'],
        'position': 0,
        'limit': [-0.94, 1.38],
        'multiplier': speed,
    },
    'joint4': {
        'keys': ['c', 'v'],
        'position': 0,
        'limit': [-1.79, 2.04],
        'multiplier': speed,
    },
    'joint5': {
        'keys': ['d', 'f'],
        'position': 0,
        'limit': [-1.79, 2.04],
        'multiplier': speed,
    },
    'finger-l-joint': {
        'keys': ['e', 'r'],
        'position': 0,
        'limit': [-0.03, 0],
        'multiplier': 0.001,
    },
}

# TODO: get one message from joint_states, assign joints automatically?
positions = {
    'joint1': 0,
    'joint2': 0,
    'joint3': 0,
    'joint4': 0,
    'joint5': 0,
    'finger-l-joint': 0,
}


def getKey():
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key


class Controller:
    def __init__(self):
        pass

    def _getKey(self):
        tty.setraw(sys.stdin.fileno())
        rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
        if rlist:
            key = sys.stdin.read(1)
        else:
            key = ''

        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
        return key

    def _updateJointStates(self, joint_states=None):
        joint_states = joint_states or self.joint_states
        if joint_states is None:
            return
        self.pub_joint_states

if __name__ == "__main__":
    settings = termios.tcgetattr(sys.stdin)

    rospy.init_node('x_series_teleop', anonymous=False)

    # joint_prefix = rospy.get_param('~joint_prefix')
    topic = rospy.get_param('~topic', 'joint_state_listener')

    pub_joint_states = rospy.Publisher(topic, JointState, queue_size=1)
    print("Publishing on topic: " + topic)

    rate = rospy.Rate(100) # 100Hz

    # TODO: Sync rate with the joint_state_listener
    for i in range(0, 8):
        msg = JointState()
        msg.header = Header()
        msg.header.stamp = rospy.Time.now()
        msg.name = positions.keys()
        msg.position = positions.values()
        msg.velocity = []
        msg.effort = []

        if debug:
            print msg

        pub_joint_states.publish(msg)
        rospy.sleep(0.15)

    try:
        print usage
        while not rospy.is_shutdown():
            key = getKey()
            if key == '\x03':
                break
            joint = None
            for item in joint_bindings.items():
                if key in item[1]['keys']:
                    joint = item[0]
                    break
            if joint is not None:
                values = joint_bindings[joint]
                keys = values['keys']
                # If the index is zero, set the pos to -1
                pos = keys.index(key) or -1

                limit = values['limit']
                position = values['position'] + values['multiplier'] * pos

                if position < limit[0]:
                    position = limit[0]
                elif position > limit[1]:
                    position = limit[1]

                joint_bindings[joint]['position'] = position

                msg = JointState()
                msg.header = Header()
                msg.header.stamp = rospy.Time.now()
                msg.name = [joint]
                msg.position = [position]
                msg.velocity = []
                msg.effort = []

                if debug:
                    print msg

                pub_joint_states.publish(msg)

            rate.sleep()
    except Exception as e:
        raise

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
