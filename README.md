# ROS X-Series Common Packages
Common ROS packages for the Interbotix X-Series robot arms.

-----

# About
This repository consists of the following ROS packages:
- `interbotix_x-series`: X-Series metapackage.
- `interbotix_x-series_bringup`: Launch files and configurations for launching the robot arms and features.
- `interbotix_x-series_controller`: Controller config and code to control physical and simulated robot arms.
- `interbotix_x-series_description`: Common files for the robot models and views.
