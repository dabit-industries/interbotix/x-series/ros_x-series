# Trossen X-Series Description
This repo contains common meshes, launch files, configurations, and robot description files related to the [Trossen X-Series Robot Arms](https://www.trossenrobotics.com/interbotix-x-series-robot-arms).

---

## Resources
- Robotis Dynamixel Meshes are property of Robotics, Inc. and were obtained on the Robotis website: http://en.robotis.com/service/downloadpage.php?ca_id=7010
